<?php

return [

    'helpscout' => [
        /*
        |--------------------------------------------------------------------------
        | Application ID
        |--------------------------------------------------------------------------
        |
        | Get this value from the `/users/apps/{userId}/{appSlug}` page within
        | the Help Scout UI. This field is required if you are using the
        | `client_credentials` grant.
        |
        */
        'appId' => env('HS_APP_ID', ''),

        /*
        |--------------------------------------------------------------------------
        | Application Secret
        |--------------------------------------------------------------------------
        |
        | Get this value from the My Apps page within the Help Scout UI. This field
        | is required if you are using the `client_credentials` grant.
        |
        */
        'appSecret' => env('HS_APP_SECRET', ''),

    ],

    'appstoreconnect' => [
        /*
        |--------------------------------------------------------------------------
        | Master Shared Secret
        |--------------------------------------------------------------------------
        |
        | This is the shared secret to access the receipt data. You can view the 
        | secret for all your apps by:
        | - logging in to AppStore Connect
        | - Click 'My Apps'
        | - Click the three dots in the top left
        | - Click 'Master Shared Secret'
        |
        */
        'sharedSecret' => env('ASC_SECRET'),
    ]

];
