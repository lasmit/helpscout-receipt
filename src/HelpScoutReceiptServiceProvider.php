<?php

namespace Lasmit\HelpScoutReceipt;

use Illuminate\Support\ServiceProvider;
use HelpScout\Api\ApiClient;

class HelpScoutReceiptServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Lasmit\HelpScoutReceipt\Http\Controllers\HelpScoutReceiptController');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/api.php'); 
        $this->publishes([
            __DIR__.'/../config/helpscoutreceipt.php' => config_path('helpscoutreceipt.php')            
        ]);  
    }
}
