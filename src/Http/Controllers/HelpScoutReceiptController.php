<?php

namespace Lasmit\HelpScoutReceipt\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use HelpScout\Api\Webhooks\IncomingWebhook;
use HelpScout\Api\ApiClientFactory;
use HelpScout\Api\Customers\Customer;
use HelpScout\Api\Conversations\Threads\NoteThread;
use HelpScout\Api\Conversations\Threads\Attachments;

class HelpScoutReceiptController extends Controller
{
    public function index(Request $request)
    {
        Log::debug("HelpScoutReceiptController::index");

        $body = $request->getContent();
        $secret = config('helpscoutreceipt.helpscout.appSecret');

        $webhook;
        try {
            $webhook = IncomingWebhook::makeFromGlobals($secret);
        } catch (\HelpScout\Api\Exception\InvalidSignatureException $e) {
            Log::error("Error: " . $e);
            //return success because if we return an error them the hook will automatically stop calling after a while
            return $this->respondWithSuccessMessage("Error");
        }

        $eventType = $webhook->getEventType();

        $conversationStatus = [""];

        if ($eventType === 'convo.created') {
            Log::debug("Convo Created");
            $conversation = $webhook->getConversation();

            if (!$conversation) {
                return $this->respondWithSuccessMessage("Conversation not found");
            }
            
            $convo = $webhook->getDataObject();
            if (is_null($convo->threads)) {
                $this->respondWithSuccessMessage("No threads");
                return;
            }
            
            foreach ($convo->threads as $thread) {
                if (is_null($thread->attachments)) {
                    continue;
                }
                foreach ($thread->attachments as $attachment) {
                    Log::debug("Perusing: " . $attachment->filename);

                    if ($attachment->filename == "receipt.txt" && $attachment->url) {
                        $receipt = \file_get_contents($attachment->url);
                        Log::debug("Fetching receipt data");
                        $receiptData = $this->getReceiptData($receipt);
                        Log::debug("Receipt data returned. Status: " . $receiptData->status);

                        if ($receiptData->status == 21002) {
                            Log::warning("Invalid response from Apple");
                            $conversationStatus[] = $this->addNote("Invalid response from Apple", $conversation);
                            continue;
                        }

                        if (isset($receiptData->receipt) == false) {
                            $conversationStatus[] = $this->addNote("No receipt in receipt data", $conversation);
                            continue;
                        }

                        if ($receiptData->receipt->original_purchase_date) {
                            $dataToSend['First Downloaded'] = $receiptData->receipt->original_purchase_date;
                        }

                        if ($receiptData->receipt->in_app) {
                            $dataToSend['Purchases'] = $receiptData->receipt->in_app;
                        }

                        $conversationStatus[] = $this->addNote($dataToSend, $conversation);
                    }

                    if ($attachment->filename == "logs.zip" && $attachment->url) {
                        Log::debug("Unzipping logs");
                        $fileContents = \file_get_contents($attachment->url);
                        \file_put_contents("logs.zip", $fileContents);

                        $zip = \zip_open("logs.zip");
                        if (!\is_resource($zip)) {
                            Log::debug("Could not open zip file");
                            continue;
                        }

                        while ($file = \zip_read($zip)) {
                            Log::debug("Reading $file");
                            $logContents = \zip_entry_read($file);
                            \preg_match('/[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}-[0-9A-F]{4}-[0-9A-F]{16}/', $logContents, $matches);
                            if ($matches) {
                                $hash = \md5($matches[0] . "edargruupgrade");
                                $url = "progresstlb://upgrade?" . $hash;
                                $conversationStatus[] = $this->addNote($url, $conversation);
                                break;
                            } else {
                                Log::debug("Could not find a UUID");
                            }
                        }
                    }
                }
            }
        }

        Log::debug($conversationStatus);
        return $this->respondWithSuccessMessage("Completed");
    }

    function zipFileErrMsg($errno)
    {
        // using constant name as a string to make this function PHP4 compatible
        $zipFileFunctionsErrors = array(
            'ZIPARCHIVE::ER_MULTIDISK' => 'Multi-disk zip archives not supported.',
            'ZIPARCHIVE::ER_RENAME' => 'Renaming temporary file failed.',
            'ZIPARCHIVE::ER_CLOSE' => 'Closing zip archive failed',
            'ZIPARCHIVE::ER_SEEK' => 'Seek error',
            'ZIPARCHIVE::ER_READ' => 'Read error',
            'ZIPARCHIVE::ER_WRITE' => 'Write error',
            'ZIPARCHIVE::ER_CRC' => 'CRC error',
            'ZIPARCHIVE::ER_ZIPCLOSED' => 'Containing zip archive was closed',
            'ZIPARCHIVE::ER_NOENT' => 'No such file.',
            'ZIPARCHIVE::ER_EXISTS' => 'File already exists',
            'ZIPARCHIVE::ER_OPEN' => 'Can\'t open file',
            'ZIPARCHIVE::ER_TMPOPEN' => 'Failure to create temporary file.',
            'ZIPARCHIVE::ER_ZLIB' => 'Zlib error',
            'ZIPARCHIVE::ER_MEMORY' => 'Memory allocation failure',
            'ZIPARCHIVE::ER_CHANGED' => 'Entry has been changed',
            'ZIPARCHIVE::ER_COMPNOTSUPP' => 'Compression method not supported.',
            'ZIPARCHIVE::ER_EOF' => 'Premature EOF',
            'ZIPARCHIVE::ER_INVAL' => 'Invalid argument',
            'ZIPARCHIVE::ER_NOZIP' => 'Not a zip archive',
            'ZIPARCHIVE::ER_INTERNAL' => 'Internal error',
            'ZIPARCHIVE::ER_INCONS' => 'Zip archive inconsistent',
            'ZIPARCHIVE::ER_REMOVE' => 'Can\'t remove file',
            'ZIPARCHIVE::ER_DELETED' => 'Entry has been deleted',
        );

        $errmsg = 'unknown';
        foreach ($zipFileFunctionsErrors as $constName => $errorMessage) {
            if (defined($constName) and constant($constName) === $errno) {
                return 'Zip File Function error: ' . $errorMessage;
            }
        }
        return 'Zip File Function error: unknown';
    }

    /**
     * Verify a receipt and return receipt data
     *
     * @param   string  $receipt    Base-64 encoded data
     * @param   bool    $isSandbox  Optional. True if verifying a test receipt
     * @throws  Exception   If the receipt is invalid or cannot be verified
     * @return  array       Receipt info (including product ID and quantity)
     */
    private function getReceiptData($receipt, $isSandbox = false)
    {
        // determine which endpoint to use for verifying the receipt
        if ($isSandbox) {
            $endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
        } else {
            $endpoint = 'https://buy.itunes.apple.com/verifyReceipt';
        }

        $postData = json_encode(
            array(
                'receipt-data' => $receipt,
                'password' => config('helpscoutreceipt.appstoreconnect.sharedSecret')
            )
        );

        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $response = curl_exec($ch);
        $errno    = curl_errno($ch);
        $errmsg   = curl_error($ch);
        curl_close($ch);

        if ($errno != 0) {
            throw new \Exception($errmsg, $errno);
        }

        $data = json_decode($response);

        if (!is_object($data)) {
            throw new \Exception('Invalid response data: ' . $response);
        }

        return $data;
    }

    private function addNote($data, $conversation)
    {
        $flatData = print_r($data, true);
        Log::debug("Adding note: " . $flatData);

        $client = ApiClientFactory::createClient();
        $client->useClientCredentials(
            config('helpscoutreceipt.helpscout.appId'),
            config('helpscoutreceipt.helpscout.appSecret')
        );

        $customer = new Customer();
        $customer->setId($conversation->getCustomer()->getId());
        $thread = new NoteThread();
        // $thread->setCustomer($customer);
        $thread->setText($flatData);

        try {
            $client->threads()->create($conversation->getId(), $thread);
        } catch (\HelpScout\Api\Exception\ValidationErrorException $e) {
            Log::error("Error: " . $e->getError());
            return "Error: " . $e->getError();
        }

        return "Note added successfully: " . $flatData;
    }
}
