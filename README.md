# Helpscout Receipt

When a new ticket is created in HelpScout, automatically take the attached receipt and lookup values from the App Store and then add the details as a note on the ticket.

## Installation

### In HelpScout

- Login to your account

- From the top menu choose Manage > Users
- Select your user
- Select 'My Apps' from the left
- Click 'Create My App'
- For the app name enter `helpscout-receipt`
- For the redirection url enter `http://google.com` (This url is required on this form, but not actually used)
- Copy the App ID and App Secret to 

- From the top menu choose to open Manage > Apps in a new tab
- Choose Webhooks
- Choose Install
- Copy you the App Secret from the My Apps tab
- Set the callback url to https://<yourserver>/api/v1/helpscout-receipt
- Select the 'Conversation Created' Event
- Save
- Leave both tabs open


### In your code

- Run `composer require lasmit/helpscout-receipt`
- Run `php artisan vendor:publish` and choose `Provider: Lasmit\HelpScoutReceipt\HelpScoutReceiptServiceProvider`
- In your project, edit `config/helpscoutreceipt.php`
- Set helpscout > appsecret to be the **secret key** you created in the HelpScout section above
- Set helpscout > appid to be the app id from the app you created above 

TODO: finish this

## Usage

TODO: finish this